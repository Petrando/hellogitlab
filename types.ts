import {v4 as uuid} from 'uuid';

export interface ToDo {
  readonly id: string;
  task:string,
  complete:boolean
}

export enum todoFilter {
  All = 'ALL',
  Complete = 'COMPLETE',
  Incomplete = 'INCOMPLETE'
}