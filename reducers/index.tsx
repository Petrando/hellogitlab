import {ToDo, todoFilter} from '../types';

//const todoFilterOption = todoFilter.All | todoFilter.Complete | todoFilter.Incomplete;

interface filterAction {
  type: 'SHOW_ALL' | 'SHOW_COMPLETE' | 'SHOW_INCOMPLETE' | Error
}

export const filterReducer = (state, action:filterAction) => {  
  switch (action.type) {
    case 'SHOW_ALL':
      return todoFilter.All;
    case 'SHOW_COMPLETE':
      return todoFilter.Complete;
    case 'SHOW_INCOMPLETE':
      return todoFilter.Incomplete;
    default:
      throw new Error();
  }
};

interface todoAction {
  type:'DO_TODO' | 'UNDO_TODO' | 'ADD_TODO' | 'DELETE_TODO', 
  id:string,
  task?:string
}

export const todoReducer = (state, action:todoAction) => {  
  switch (action.type) {
    case 'DO_TODO':
      return state.map((todo:ToDo) => {
        if (todo.id === action.id) {
          return { ...todo, complete: true };
        } else {
          return todo;
        }
      });
    case 'UNDO_TODO':
      return state.map((todo:ToDo) => {
        if (todo.id === action.id) {
          return { ...todo, complete: false };
        } else {
          return todo;
        }
      });
    case 'ADD_TODO':
      return state.concat({
        task: action.task,
        id: action.id,
        complete: false,
      });
    case 'DELETE_TODO':
      return state.slice().filter((todo:ToDo) => todo.id!==action.id);
    default:
      throw new Error();
  }
};
