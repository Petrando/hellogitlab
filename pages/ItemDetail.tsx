import React, { useState, useEffect } from 'react';
import {v4 as uuid} from 'uuid';
import Head from 'next/head'
import styles from '../styles/Home.module.css'
import {Backdrop, Button, CircularProgress, Container, Grid, Paper, TextField, Typography, makeStyles } from '@material-ui/core/';
import {Card, CardActionArea, CardActions, CardContent, CardMedia} from '@material-ui/core/';
import Pagination from '@material-ui/lab/Pagination';
import Link from 'next/link';
import { useRouter } from 'next/router';

import {API} from '../config'

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));

interface ItemData {
  Description:string, 
  ID:number, 
  LevelItem:number, 
  Name:string, 
  image:string
}

export default function ItemDetail(): JSX.Element {   
  const classes = useStyles();

  const [itemData, setItemData] = useState<null | ItemData>(null);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const router = useRouter()

  useEffect(()=>{   
    getItemData();    
  }, []);

  const getItemData = async () => {
    if(typeof router.query.ID === "undefined"){
      return;
    }
    setIsLoading(true);    
    const fetchResult = await(fetch(`${API}/item/${router.query.ID}?columns=ID,Name,Description,LevelItem`, { mode: 'cors' }));
    const data = await(fetchResult.json());
    setItemData({...data, image:`${API}/${router.query.Icon}`});
    setIsLoading(false);
  }    

  return (
    <div className={styles.containerFromTop}>
      <Head>
        <title>Item Detail</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
       <Backdrop className={classes.backdrop} open={isLoading} >
        <CircularProgress color="inherit" />
      </Backdrop>
      <main className={styles.mainFromTop}>
         <p style={{textAlign:'left'}}>
          <Link href="/ApiAccess">
            <a>← Back to API Access</a>
          </Link>
          <Link href="/">
            <a>← Back to home</a>
          </Link>
        </p>              
        <Container>  
          {
            itemData!==null && 
            <Card style={{height:"100%"}}>            
              <CardActionArea style={{ display:"flex", flexDirection:"column", justifyContent:"space-between"}}>
          
                <div style={{ paddingTop:"10px", display:"flex", flexDirection:"row", alignItems:"center", justifyContent:"space-around"}}>
                  <CardMedia
                    style={{width:"40px", height:"40px"}}
                    component="img"
                    alt={itemData.Name}                
                    image={itemData.image}
                    title={itemData.Name}
                  />
                  <Typography gutterBottom variant="h5">
                    {itemData.Name}
                  </Typography>
                </div>     
                <CardContent>
                  <Typography gutterBottom variant="subtitle1">
                    Description
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    {itemData.Description}
                  </Typography>
                  <Typography gutterBottom variant="subtitle2">
                    Item Level : {itemData.LevelItem}
                  </Typography>                  
                </CardContent>                     
              </CardActionArea>
              <CardActions style={{width:"100%"}}>          
                <Link href={"/ApiAccess"} >
                  <a>← Back to API Access</a>
                </Link>
              </CardActions> 
            </Card>
          }                    
        </Container>         
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="/vercel.svg" alt="Vercel Logo" className={styles.logo} />
        </a>
      </footer>
    </div>
  )
}