import React, { useState, useReducer  } from 'react';
import {v4 as uuid} from 'uuid';
import Head from 'next/head'
import styles from '../styles/Home.module.css'
import { FaHandPaper } from "react-icons/fa";
import Container from '@material-ui/core/Container';
import Link from 'next/link';

import {todoReducer, filterReducer} from '../reducers/index';
import Filter from '../components/Filter';
import TodoList from '../components/TodoList';
import AddTodo from '../components/AddTodo';

import TodoContext from '../contexts/TodoContext';

import { ToDo, todoFilter} from '../types';

const initialTodos: ToDo[] = [
  {
    id: uuid(),
    task: 'Learn React',
    complete: true,
  },
  {
    id: uuid(),
    task: 'Learn Firebase',
    complete: true,
  },
  {
    id: uuid(),
    task: 'Learn GraphQL',
    complete: false,
  },
];

export default function TodoApp(): JSX.Element {
  const [todos, dispatchTodos] = useReducer(todoReducer, initialTodos);  
  const [filter, dispatchFilter] = useReducer(filterReducer, todoFilter.All);  

  const filteredTodos = todos.filter((todo:ToDo) => {
    if (filter === todoFilter.All) {
      return true;
    }
 
    if (filter === todoFilter.Complete && todo.complete) {
      return true;
    }
 
    if (filter === todoFilter.Incomplete && !todo.complete) {
      return true;
    }
 
    return false;
  });

  return (
    <div className={styles.container}>
      <Head>
        <title>Todo App (Formerly Hello World)</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          {'\"Hello,  World!'} <FaHandPaper className={styles.wave} />{'\"'} {'is now a \"Todo App\"'}
        </h1>        
        <Container>
          <TodoContext.Provider value={dispatchTodos}>
            <AddTodo />
            <Filter dispatch={dispatchFilter} filter={filter} />
            <TodoList todos={filteredTodos} filter={filter} />        
          </TodoContext.Provider>          
        </Container>  
        <p style={{textAlign:'left'}}>
          <Link href="/">
            <a>← Back to home</a>
          </Link>
        </p>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="/vercel.svg" alt="Vercel Logo" className={styles.logo} />
        </a>
      </footer>
    </div>
  )
}
