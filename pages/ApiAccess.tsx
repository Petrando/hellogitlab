import React, { useState, useEffect, Component } from 'react';
import Head from 'next/head'
import styles from '../styles/Home.module.css'
import {Backdrop, Button, CircularProgress, Container, Grid, Paper, TextField, Typography, makeStyles } from '@material-ui/core/';
import {Card, CardActionArea, CardActions, CardContent, CardMedia} from '@material-ui/core/';
import Pagination from '@material-ui/lab/Pagination';
import Link from 'next/link';

import {API} from '../config'

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));

const pageTotal = 332;

interface ItemInterface {
  ID:string,
  Icon:string,
  Name:string,
  Url:string
}

export default function ApiAccess(): JSX.Element { 
  const classes = useStyles();

  const [items, setItems] = useState<ItemInterface[]>([{ID:'x', Icon:'x', Name:'no-render', Url:'x'}]);
  const [nameFilter, setNameFilter] = useState<string>('');
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [isLoading, setIsLoading] = useState<boolean>(false);  

  useEffect(()=>{   
    getItemsPerPage();       
  }, [currentPage]);

  const getItemsPerPage = async ():Promise<void> => {    
    setIsLoading(true);    
    const fetchResult = await(fetch(`${API}/item?Page=${currentPage}`, { mode: 'cors' }));
    const data = await(fetchResult.json());
    setItems(data.Results);
    setIsLoading(false);
  }

  const handlePageChange = (_event, value):void => {
    setCurrentPage(value);
    setNameFilter('');
  };

  const filteredItems = ():ItemInterface[] => { 
    if(items[0].Name==="no-render"){
      return [];
    }
    if(nameFilter===""){      
      return items;
    }else{      
      return items.filter(item => {        
        return item.Name.toUpperCase().includes(nameFilter.toUpperCase())})
    }        
  }
  
  //const paths = () => filteredItems.map(i => i.ID)

  return (
    <Container>
      <Head>
        <title>API access play</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Backdrop className={classes.backdrop} open={isLoading} >
        <CircularProgress color="inherit" />
      </Backdrop>
      <main className={styles.mainFromTop}>
         <p style={{textAlign:'left'}}>
          <Link href="/">
            <a>← Back to home</a>
          </Link>
        </p>
        <h1 className={styles.title}>
          Final Fantasy XIV Item List 
        </h1>        
        <Container>
          <Pagination count={pageTotal} variant="outlined" shape="rounded" showFirstButton showLastButton
            onChange={handlePageChange}
          />
          <div>
          <TextField 
            style={{ margin: 8 }}
            fullWidth
            placeholder="Filter item name - in this page only."
            value={nameFilter}
            onChange={(e)=>setNameFilter(e.target.value)}
          />
          </div>
          <Grid container spacing={2}>
            { 
              !isLoading && 
              filteredItems().length > 0 &&              
              filteredItems().map((item, i)=>
                                  <AnItem key={item.ID} item={item} 
                                    
                                  />)
            }
          </Grid>      
        </Container>         
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="/vercel.svg" alt="Vercel Logo" className={styles.logo} />
        </a>
      </footer>
    </Container>
  )
}

//paths={items.map(i => i.ID)}
interface AnItemProps {
  item:ItemInterface
}
//<Link href={{ pathname: '/ItemDetail', query: { ID, Icon } }} >
const AnItem = ({item:{ID, Icon, Name, Url}}:AnItemProps):JSX.Element => {  

  return (
    <Grid item lg={2} md={3} sm={6} xs={12} >
    <Link href={{ pathname: `/ItemDetail`, query: { ID, Icon } }} ><a>
    <Card style={{height:"100%", display:"flex", flexDirection:"column", justifyContent:"space-between"}}>
      <CardActionArea >
        <div style={{width:"100%", display:"flex", flexDirection:"row", alignItems:"center", justifyContent:"space-around"}}>
          <ItemImage Name={Name} Icon={Icon} />
          <Typography gutterBottom variant="h6">
            {Name}
          </Typography>
        </div>                  
      </CardActionArea>
      <CardActions style={{width:"100%"}}> 
        <Button size="small" color="primary">                     
          Click to View       
        </Button>
      </CardActions> 
    </Card>
    </a></Link>
  </Grid>
  )  
}

interface ItemImageProps {
  Name:string,
  Icon:string
}

const ItemImage = ({Name, Icon}:ItemImageProps):JSX.Element => {
  const [imageLoaded, setImageLoaded] = useState<boolean>(false);

  return (
    <>
      {
        !imageLoaded &&
        Name!=="" &&
        <CircularProgress />          
      }   
      <CardMedia
        style={{width:"40px", height:"40px", display:imageLoaded?"inline-block":"none"}}
        component="img"
        alt={`Image of ${Name}`}                
        image={`${API}${Icon}`}
        title={Name}
        onLoad={()=>setImageLoaded(true)}
      />
    </> 
  )
}