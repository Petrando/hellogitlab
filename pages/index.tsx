import React from 'react';
import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Container from '@material-ui/core/Container';
import Link from 'next/link'

export default function Home(): JSX.Element {  

  return (
    <div className={styles.container}>
      <Head>
        <title>Gitlab Play</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>          
        <Container>     
          <h1 className={styles.title}>
            My Practices with React and Typescript
          </h1>      
          <div className={styles.grid}>
            <Link href="/TodoApp">
              <a className={styles.card}>              
                <h3>To Do App &rarr;</h3>              
                <p>First time playing with Gitlab and Typescript.</p>
              </a>
            </Link>

            <Link href="/ApiAccess">
              <a className={styles.card}>
                <h3>API Access &rarr;</h3>
                <p>Trying to access some API endpoint!</p>
              </a>
            </Link>           
          </div>
        </Container>        
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="/vercel.svg" alt="Vercel Logo" className={styles.logo} />
        </a>
      </footer>
    </div>
  )
}
