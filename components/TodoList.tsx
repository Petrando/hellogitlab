import React, {Component, useContext} from 'react';
import TodoContext from '../contexts/TodoContext';
import {Button, Container, Checkbox, FormControlLabel, FormGroup, Paper, Typography} from '@material-ui/core';
import { FaTrash } from "react-icons/fa";
import {ToDo, todoFilter} from '../types';
import 'fontsource-roboto';

interface TodoListProps {
	todos:ToDo[],
	filter: todoFilter.All | todoFilter.Complete | todoFilter.Incomplete
}

const TodoList = ({ todos, filter}:TodoListProps):JSX.Element => (
  <Container>  	
    {todos.length > 0 && todos.map(todo => (
      <TodoItem key={todo.id} todo={todo} />
    ))}
    {todos.length===0 && 
    	<Typography variant="subtitle1" gutterBottom>
        	{
        		filter===todoFilter.All?'Nothing to do...':
        		filter===todoFilter.Complete?'Nothing has been done yet..':
        		'Nothing has not been done..'
        	}
      	</Typography>
    }
  </Container>
);

interface TodoItemProps {
	todos:ToDo,	
}
 
const TodoItem = ({ todo }):JSX.Element => {
  const dispatch = useContext(TodoContext);

  const handleChange = ():void =>
    dispatch({
      type: todo.complete ? 'UNDO_TODO' : 'DO_TODO',
      id: todo.id,
    });
 
  const handleDelete = ():void => {
 	dispatch({type:'DELETE_TODO', id:todo.id})
  }

  return (
    <Paper>
      <FormGroup row style={{marginLeft:'3px', display:'flex', flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>      	
      	<FormControlLabel
        	control={<Checkbox checked={todo.complete} onChange={handleChange}  />}
        	label={todo.task}
      	/>
      	<Button onClick={handleDelete}>
      		<FaTrash style={{fontSize:"20px"}} />
      	</Button>
      </FormGroup>
    </Paper>
  );
};


export default TodoList;