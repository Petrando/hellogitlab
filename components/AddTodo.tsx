import React, {Component, useState, useContext} from 'react';
import {v4 as uuid} from 'uuid';
import {Button, Container, TextField} from '@material-ui/core';
import TodoContext from '../contexts/TodoContext';

const AddTodo = ():JSX.Element => {
  const dispatch = useContext(TodoContext);
  const [task, setTask] = useState<string>('');
 
  const handleSubmit = (event:React.SyntheticEvent):void => {    
    
    if (task) {
      dispatch({ type: 'ADD_TODO', task, id: uuid() });
    }
 
    setTask('');    

    event.preventDefault();
  };
 
  const handleChange = (event:React.ChangeEvent<HTMLInputElement>):void => setTask(event.currentTarget.value);
 
  return (
    <Container style={{marginBottom:'5px'}} >
      <form onSubmit={handleSubmit}>
        <TextField value={task} onChange={handleChange} placeholder={"Wat do you wanna do?"} />
        <Button type="submit" variant="contained" color="primary">Add Todo</Button>
      </form>
    </Container>
  );
};

export default AddTodo;