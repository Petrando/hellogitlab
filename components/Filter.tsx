import React, {Component} from 'react';
import {Button, Container} from '@material-ui/core';
import {todoFilter} from '../types';

//const todoFilterOption = todoFilter.All | todoFilter.Complete | todoFilter.Incomplete

interface FilterProps {
  dispatch:any,
  filter: string
}

const Filter = ({ dispatch, filter }:FilterProps):JSX.Element => {
  const handleShowAll = ():void => {
    dispatch({ type: 'SHOW_ALL' });
  };
 
  const handleShowComplete = ():void => {
    dispatch({ type: 'SHOW_COMPLETE' });
  };
 
  const handleShowIncomplete = ():void => {
    dispatch({ type: 'SHOW_INCOMPLETE' });
  };
 
  return (
    <Container style={{marginBottom:'5px'}}>
      <Button variant="contained" color={filter===todoFilter.All?"primary":"secondary"} onClick={handleShowAll} >
        Show All
      </Button>
      <Button variant="contained" color={filter===todoFilter.Complete?"primary":"secondary"} onClick={handleShowComplete}>
        Show Complete
      </Button>
      <Button variant="contained" color={filter===todoFilter.Incomplete?"primary":"secondary"} onClick={handleShowIncomplete}>
        Show Incomplete
      </Button>
    </Container>
  );
};

export default Filter;